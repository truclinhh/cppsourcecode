bool abbreviation_recursion(std::vector<std::vector<bool>> *memo,
                            std::vector<std::vector<bool>> *visited,
                            const std::string &str, const std::string &result,
                            uint32_t str_idx = 0, uint32_t result_idx = 0) {
    bool ans = memo->at(str_idx).at(result_idx);
    if (str_idx == str.size() && result_idx == result.size()) {
        return true;
    } else if (str_idx == str.size() && result_idx != result.size()) {
        // result `t` is not converted, return false
        return false;
    } else if (!visited->at(str_idx).at(result_idx)) {
        /**
         * `(str[i] == result[j])`: if str char at position i is equal to
         * `result` char at position j, then s character is a capitalized one,
         * move on to next character `str[i] - 32 == result[j]`:
         * if `str[i]` character is lowercase of `result[j]` then explore two
         * possibilites:
         * 1. convert it to capitalized letter and move both to next pointer
         * `(i + 1, j + 1)`
         * 2. Discard the character `(str[i])` and move to next char `(i + 1,
         * j)`
         */
        if (str[str_idx] == result[result_idx]) {
            ans = abbreviation_recursion(memo, visited, str, result,
                                         str_idx + 1, result_idx + 1);
        } else if (str[str_idx] - 32 == result[result_idx]) {
            ans = abbreviation_recursion(memo, visited, str, result,
                                         str_idx + 1, result_idx + 1) ||
                  abbreviation_recursion(memo, visited, str, result,
                                         str_idx + 1, result_idx);
        } else {
            // if `str[i]` is uppercase, then cannot be converted, return
            // `false`
            // else `str[i]` is lowercase, only option is to discard this
            // character
            if (str[str_idx] >= 'A' && str[str_idx] <= 'Z') {
                ans = false;
            } else {
                ans = abbreviation_recursion(memo, visited, str, result,
                                             str_idx + 1, result_idx);
            }
        }
    }
    (*memo)[str_idx][result_idx] = ans;
    (*visited)[str_idx][result_idx] = true;
    return (*memo)[str_idx][result_idx];
}
