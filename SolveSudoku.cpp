bool solveSudoku(std::array<std::array<int, V>, V> &mat,
                 const std::array<std::array<int, V>, V> &starting_mat, int i,
                 int j) {
    /// Base Case
    if (i == 9) {
        /// Solved for 9 rows already
        printMat<V>(mat, starting_mat, 9);
        return true;
    }

    /// Crossed the last  Cell in the row
    if (j == 9) {
        return solveSudoku<V>(mat, starting_mat, i + 1, 0);
    }

    /// Blue Cell - Skip
    if (mat[i][j] != 0) {
        return solveSudoku<V>(mat, starting_mat, i, j + 1);
    }
    /// White Cell
    /// Try to place every possible no
    for (int no = 1; no <= 9; no++) {
        if (isPossible<V>(mat, i, j, no, 9)) {
            /// Place the 'no' - assuming a solution will exist
            mat[i][j] = no;
            bool solution_found = solveSudoku<V>(mat, starting_mat, i, j + 1);
            if (solution_found) {
                return true;
            }
            /// Couldn't find a solution
            /// loop will place the next `no`.
        }
    }
    /// Solution couldn't be found for any of the numbers provided
    mat[i][j] = 0;
    return false;
}
