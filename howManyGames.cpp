int howManyGames(int p, int d, int m, int s)
{
    // Return the number of games you can buy
    int count = 0;
    if (p > s)
    {
        return 0;
    }
    else if (p == s)
    {
        return 1;
    }
    while (s > 0)
    {
        s -= p;
        if (p - d <= m)
        {
            p = m;
        }
        else if (p - d > m)
        {
            p -= d;
        }
        if (s >= 0)
        {
            count++;
        }
    }
    return count;
}
