std::string decrypt(const std::string& text, bool bReturnUppercase = false) {
    std::string result;

    // split words seperated by spaces into a vector array
    std::vector<std::string> word_array;
    std::stringstream sstream(text);
    std::string word;
    while (sstream >> word) {
        word_array.push_back(word);
    }

    for (auto& i : word_array) {
        std::replace(i.begin(), i.end(), '-', ' ');
        std::vector<std::string> text_array;

        std::stringstream ss(i);
        std::string res_text;
        while (ss >> res_text) {
            text_array.push_back(res_text);
        }

        for (auto& i : text_array) {
            result += a1z26_decrypt_map[stoi(i)];
        }

        result += ' ';
    }
    result.pop_back();  // remove any leading whitespace

    if (bReturnUppercase) {
        std::transform(result.begin(), result.end(), result.begin(), ::toupper);
    }
    return result;
}
