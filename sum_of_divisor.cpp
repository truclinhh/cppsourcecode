int sum_of_divisor(int num) {
    // Variable to store the sum of all proper divisors.
    int sum = 0;
    // Below loop condition helps to reduce Time complexity by a factor of
    // square root of the number.
    for (int div = 2; div * div <= num; ++div) {
        // Check 'div' is divisor of 'num'.
        if (num % div == 0) {
            // If both divisor are same, add once to 'sum'
            if (div == (num / div)) {
                sum += div;
            } else {
                // If both divisor are not the same, add both to 'sum'.
                sum += (div + (num / div));
            }
        }
    }
    return sum + 1;
}

