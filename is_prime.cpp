bool is_prime(T num) {
    bool result = true;
    if (num <= 1) {
        return false;
    } else if (num == 2 || num == 3) {
        return true;
    } else if ((num % 2) == 0 || num % 3 == 0) {
        return false;
    } else {
        for (T i = 5; (i * i) <= (num); i = (i + 6)) {
            if ((num % i) == 0 || (num % (i + 2) == 0)) {
                result = false;
                break;
            }
        }
    }
    return (result);
}
